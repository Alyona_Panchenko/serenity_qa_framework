package jbehave.scenariosteps;

import core.common.pages.HomePage;
import core.common.utils.WebDriverUtil;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.reports.html.ExampleTable;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebDriver;
import serenity.steps.HomePageSteps;
import serenity.steps.ProductDetailsPageSteps;

public class ProductDetailsPageScenario {

    @Steps
    private HomePageSteps homePageSteps;

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;

    @Given("user has opened product details page: $partialLink")
    public void nagateToProductDetailsPage(final String partialUrl){
        //WebDriverUtil.
        homePageSteps.openProductDetailsPage(partialUrl);
    }

    @When("user fill in following parameters: $productParameters")
    public void fillInProductParameters(final ExamplesTable parameters){

        productDetailsPageSteps.setSize("");
        productDetailsPageSteps.setQty("");
    }

    @When("clicks 'ADD TO CART' button")
    public void clickAddToCartButton(){
        productDetailsPageSteps.clickAddToCartButton();
    }

}
