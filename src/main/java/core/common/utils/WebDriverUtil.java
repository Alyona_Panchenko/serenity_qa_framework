package core.common.utils;

import core.common.pages.AbstractPage;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Optional;
import java.util.StringJoiner;

public class WebDriverUtil {

    private static String scriptToCheck;

    private WebDriverUtil(){
    }

    public static WebDriver getDriver() {
        return ThucydidesWebDriverSupport.getDriver();
    }

    public static <T extends AbstractPage> void openPage(final T page, final String extraPath) {
        page.openAt(extraPath);
    }


    public static <T extends AbstractPage> String getDefaultUrl(final Class<T> page){
        return Optional.ofNullable(page)
                .map(item -> item.getAnnotation(DefaultUrl.class))
                .map(annotation -> annotation.value())
                .orElse(null);
    }

    public static void waitForAsyncExecution() {
        scriptToCheck = new StringJoiner(System.lineSeparator())
                .add("if ((typeof jQuery !== 'undefined') && (jQuery.active > 0)) {")
                .add(" document.cookie = 'SC.componentsAreLoaded=false'")
                .add(" document.cookie = 'SC.componentsAreLoaded=true'")
                .add("};").toString();
        final WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(e -> {
            final Object scriptResult = ((JavascriptExecutorFacade) getDriver()).executeScript(scriptToCheck);
            return Boolean.parseBoolean(String.valueOf(scriptResult));
        });
    }
}