package core.common.pages;

import core.common.utils.WebDriverUtil;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

public abstract class AbstractPage extends PageObject{

    public AbstractPage(final WebDriver driver) {
        super(driver);
        getDriver().manage().window().maximize();
    }

    public <T extends AbstractPage> void openPageByExtraPath(final Class<T> page, final String extraPath){
        final String defaultUrl = WebDriverUtil.getDefaultUrl(page);
        final String fullUrl = defaultUrl + extraPath;
        openAt(fullUrl);
    }
}
